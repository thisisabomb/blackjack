'use strict'

const fs = require('fs')
const path = require('path')
const join = path.join
const { resolve } = require('path')
const cors = require('cors')
const express = require('express')
let passport = require('passport')
let session = require('express-session')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const models = join(__dirname, 'app/models/')
const app = express()
const port = 3001
const DB = 'mongodb://127.0.0.1:27017/blackjack'


mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

passport.serializeUser(function(user, done) {
  done(null, user) //อยากส่งอะไรไปเก็บใน session
})
passport.deserializeUser(function(obj, done) {
  done(null, obj) //เอาของที่เก็บใน session มาใช้ต่อ
})

mongoose.Promise = global.Promise
mongoose.connect(DB,{ useNewUrlParser: true }).then(
  () => {
    console.log('success')
  },
  err => {
    console.log(`Error: ${err}`)
  }
)

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.indexOf('.js'))
  .forEach(file => require(join(models, file)))

require('./config/passport')(passport)

app.use(cors())
app.set('superSecret', 'server secret555')

// Config body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}))

// // app.use(cookieParser());
app.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized: true }))
app.use(passport.initialize())
app.use(passport.session())

module.exports = app


app.get('/', (req, res) => {
  res.send('please login')
})

const appRouter = require(resolve('app/controller'))
const APP_PATH =  '/'
app.use(APP_PATH, appRouter(app, passport))

app.listen(port,() => {
  console.log(`Port ${port}!`)
})
