const mongoose = require('mongoose')

let UserSchema = mongoose.Schema({
  username: { type: String, unique: true },
  password: { type: String },
  cards: {
    all: { type: Array, default: [] },
    player: { type: Array, default: [] },
    dealer: { type: Array, default: [] }
  },
  score: {
    player: { type: Number, default: 0 },
    dealer: { type: Number, default: 0 }
  }
})

mongoose.model('User', UserSchema)