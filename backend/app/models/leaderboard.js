const mongoose = require('mongoose')

let LeaderBoardSchema = mongoose.Schema({
  profile: { type: Object },
  statistics: {
    win: { type: Number, default: 0 },
    loss: { type: Number, default: 0 },
    draw: { type: Number, default: 0 },
  }
})

mongoose.model('LeaderBoard', LeaderBoardSchema)