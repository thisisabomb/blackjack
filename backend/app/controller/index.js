const { resolve } = require('path')
const path = require('path')
const join = path.join
const express = require('express')
const mongoose = require('mongoose')
const expressJwt = require('express-jwt')
const jwt = require(resolve('app/controller/jwt'))
const card = require(resolve('app/controller/cards'))

module.exports = (app, passport) => {
  const authenOptions = { session: false }
  const secretKey = app.get('superSecret')
  const authenticate = expressJwt({
    secret: secretKey
  })
  let router = express.Router()

  // Authentication //
  router.post('/signup', passport.authenticate('local-signup', authenOptions), jwt.generateToken(secretKey), jwt.respondToken)

  router.post('/signin', passport.authenticate('local-login', authenOptions), jwt.generateToken(secretKey), jwt.respondToken)

  // Blackjack
  router.put('/cards/start',
    authenticate,
    card.startBlackjack,
    card.startResponse,
  )

  router.put('/cards/hit',
    authenticate,
    card.hitBlackjack,
    card.hitResponse,
  )

  router.put('/cards/stand',
    authenticate,
    card.standBlackjack,
    card.standResponse
  )

  // LeaderBoard
  router.get('/board',
    card.listLeaderBoard
    )

  return router
}