const { resolve } = require('path')
const path = require('path')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const User = mongoose.model('User')
const jwt = require('jsonwebtoken')
const configJwt = require(resolve('config/auth')).jwt

const generateAccessToken = (key, user) => {
  return jwt.sign(
    {
      id: user.id
    },
    key,
    {
      expiresIn: configJwt.access_token_expire
    }
  )
}

exports.respondToken = (req, res) => {
  res.status(200).json({
    success: true,
    token: {
      access: req.accessToken,
      expire_in: configJwt.expire_in,
      type: configJwt.type
    }
  })
}

exports.generateToken = (key) => {
  return function (req, res, next) {
    const user = req.user
    req.accessToken = generateAccessToken(key, user)
    console.log('access token: ', req.accessToken)
    next()
  }
}
