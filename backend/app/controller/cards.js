const { resolve } = require('path')
const path = require('path')
const mongoose = require('mongoose')
const deck = require(resolve('config/cards')).deck
const User = mongoose.model('User')
const LeaderBoard = mongoose.model('LeaderBoard')
const merge = require('lodash/merge')

function shuffle(array) {
  array.sort(() => Math.random() - 0.5);
  return array
}

exports.listLeaderBoard = async (req, res, next) => {
  try {
    const filter = {
      skip: 0,
      limit: 10,
      order: '-statistics.win',
    }
    merge(filter, req.query.filter)
    const leaderBoard = await LeaderBoard
      .find()
      .sort(filter.order)
      .skip(+filter.skip)
      .limit(+filter.limit)
    res.status(200).json(leaderBoard)
  }
  catch (e) {
    
  }
}

exports.hitResponse = async (req, res, next) => {
  try{
    const { user, score } = req
    const newUpdate = await User.findOneAndUpdate({ _id: user.id }, { 'score.player': score.player }, { new: true })
    score.player > 21 ? (
      await LeaderBoard.findOneAndUpdate({ 'profile.id': user.id }, { $inc: { 'statistics.loss': 1 }}),
      res.json({ forceLoss: true, message: 'Player loss !! score over 21', playerCard: newUpdate.cards.player,  playerScore: newUpdate.score.player })
    ): res.json({ forceLoss: false, playerCard: newUpdate.cards.player,  playerScore: newUpdate.score.player })
  }
  catch (e) {
    
  }
}

exports.startResponse = async (req, res, next) => {
  try{
    const { user, score } = req
    const newUpdate = await User.findOneAndUpdate({ _id: user.id }, { 'score.dealer': score.dealer, 'score.player': score.player }, { new: true })
    res.json({ playerCard: newUpdate.cards.player, dealerCard: newUpdate.cards.dealer, playerScore: newUpdate.score.player, dealerScore: newUpdate.score.dealer })
  }
  catch (e) {

  }
}

exports.standResponse = async (req, res, next) => {
  try {
    const { user, standRes } = req
    await LeaderBoard.findOneAndUpdate({ 'profile.id': user.id }, { $inc: { 'statistics.win': standRes.win, 'statistics.loss': standRes.loss, 'statistics.draw': standRes.draw }})
      .then( async profile => {
        if(!profile) {
          await new LeaderBoard({
            profile: standRes.profile,
            statistics: {
              win: standRes.win,
              loss: standRes.loss,
              draw: standRes.draw
            }
          }).save()
        }
        res.json({ message: standRes.message })
      })
  }
  catch (e) {

  }
}

const calculateCards = async (cards) => {
  let values = []
  let amount = 0
  cards.forEach( card => {
    if( card.value === 'J' || card.value === 'Q' || card.value === 'K') {
      values.push(10)
    }
    else {
      values.push(card.value)
    }
  })
  values
    .sort(function(a, b){return a-b})
    .forEach( val => {
      if ( val + amount <= 21 && val === 11 ) amount += val
      else if ( 1 + amount <= 21 && val === 11 ) amount += 1
      else amount += val
    })
  return amount
  // req.score = amount
  // next()
}

exports.startBlackjack = async (req, res, next) => {
  try {
    const user = req.user
    let playerDeck = shuffle(deck)
    const playerCards = [ playerDeck[0], playerDeck[2] ]
    const dealerCards = [ playerDeck[1], playerDeck[3] ]
    await User.findOneAndUpdate({ _id: user.id }, { 'cards.all': playerDeck.slice(4), 'cards.player': playerCards, 'cards.dealer': dealerCards }, { new: true })
      .then( async userInfo => {
        const playerScore = await calculateCards(userInfo.cards.player)
        const dealerScore = await calculateCards(userInfo.cards.dealer)
        req.score = { player: playerScore, dealer: dealerScore }
        // req.cards = userInfo.cards.dealer
        next()
      })
  }
  catch (e) {

  }
}

exports.hitBlackjack = async (req, res, next) => {
  try {
    console.log('HIT!')
    const user = req.user
    const userInfo = await User.findOne( { _id: user.id }, { cards: 1 })
    let playerDeck = userInfo.cards.all
    let newPlayerCards = userInfo.cards.player
    newPlayerCards.push(playerDeck[0])
    await User.findOneAndUpdate({ _id: user.id }, {'cards.all': playerDeck.slice(1), 'cards.player': newPlayerCards }, { new: true })
      .then( async userInfo => {
        const playerScore = await calculateCards(userInfo.cards.player)
        req.score = { player: playerScore }
        next()
      })
  }
  catch (e) {

  }
}

exports.standBlackjack = async (req, res, next) => {
  try {
    const user = req.user
    let standRes = {
      message: '',
      win: 0,
      loss: 0,
      draw : 0,
      profile: {}
    }
    await User.findOneAndUpdate({ _id: user.id }, { score: {}, cards: {} })
      .then( userInfo => {
        standRes.profile = {
          username: userInfo.username,
          id: user.id
        }
        const playerScore = 21 - userInfo.score.player
        const dealerScore = 21 - userInfo.score.dealer
        console.log(`${playerScore} ${dealerScore}`)
        if ( playerScore < dealerScore ) {
          standRes.message = 'Player win !!!'
          standRes.win += 1
        }
        else if ( playerScore > dealerScore || userInfo.score.player > 21){
          standRes.message = 'Dealer win !!!'
          standRes.loss += 1
        }
        else {
          standRes.message = 'Draw win !!!'
          standRes.draw += 1
        }
      })
    req.standRes = standRes
    next()
  }
  catch (e) {

  }
}