const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const User = mongoose.model('User')

const generateHash = (password) => {
  let salt = bcrypt.genSaltSync(10);       // TODO random salt
  return bcrypt.hashSync(password, salt)
}

const validPass = (password, user) => {
  return bcrypt.compareSync(password, user.password)
}

exports.localSignup = async (req, username, password, done) => {
  User.findOne({ username })
    .then( async user => {
      if (!user) {
        const hash = generateHash(password)
        const user = await new User({
          username: username,
          password: hash,
        }).save()
        done(null, user)
      }
      else {
        let err = new Error()
        err.name = 'UnauthorizedError'
        err.message = 'user existed'
        err.status = 401
        return done(err) // user existed
      }
    })
    .catch(done)
}

exports.localLogin = async (req, username, password, done) => {
  User.findOne({ username })
    .then(function (user) {
      let err = null
      if (!user) {
        user = null
        err = new Error()
        err.name = 'UnauthorizedError'
        err.message = 'user not found or invalid user'
        err.status = 401
      }
      else if (!validPass(password, user)) {
        user = null
        err = new Error()
        err.name = 'UnauthorizedError'
        err.message = 'user not found or invalid user'
        err.status = 401
      }
      done(err, user)
    })
    .catch(done)
}
