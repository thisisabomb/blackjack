// let deck = ['A2','A3','A4','A5','A6','A7','A8','A9','A10','AJ','AQ','AK','AA',
//   'B2','B3','B4','B5','B6','B7','B8','B9','B10','BJ','BQ','BK','BA',
//   'C2','C3','C4','C5','C6','C7','C8','C9','C10','CJ','CQ','CK','CA',
//   'D2','D3','D4','D5','D6','D7','D8','D9','D10','DJ','DQ','DK','DA']

let deck = [{ type: 'A', value: 2 },{ type: 'A', value: 3 },{ type: 'A', value: 4 },{ type: 'A', value: 5 },{ type: 'A', value: 6 },{ type: 'A', value: 7 },{ type: 'A', value: 8 },{ type: 'A', value: 9 },{ type: 'A', value: 10 },{ type: 'A', value: 11 },{ type: 'A', value: 'J' },{ type: 'A', value: 'Q' },{ type: 'A', value: 'K' },
  { type: 'B', value: 2 },{ type: 'B', value: 3 },{ type: 'B', value: 4 },{ type: 'B', value: 5 },{ type: 'B', value: 6 },{ type: 'B', value: 7 },{ type: 'B', value: 8 },{ type: 'B', value: 9 },{ type: 'B', value: 10 },{ type: 'B', value: 11 },{ type: 'B', value: 'J' },{ type: 'B', value: 'Q' },{ type: 'B', value: 'K' },
  { type: 'C', value: 2 },{ type: 'C', value: 3 },{ type: 'C', value: 4 },{ type: 'C', value: 5 },{ type: 'C', value: 6 },{ type: 'C', value: 7 },{ type: 'C', value: 8 },{ type: 'C', value: 9 },{ type: 'C', value: 10 },{ type: 'C', value: 11 },{ type: 'C', value: 'J' },{ type: 'C', value: 'Q' },{ type: 'C', value: 'K' },
  { type: 'D', value: 2 },{ type: 'D', value: 3 },{ type: 'D', value: 4 },{ type: 'D', value: 5 },{ type: 'D', value: 6 },{ type: 'D', value: 7 },{ type: 'D', value: 8 },{ type: 'D', value: 9 },{ type: 'D', value: 10 },{ type: 'D', value: 11 },{ type: 'D', value: 'J' },{ type: 'D', value: 'Q' },{ type: 'D', value: 'K' }]

module.exports = { deck }
