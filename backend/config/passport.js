const LocalStrategy = require('passport-local').Strategy

const { resolve } = require('path')
const auth = require(resolve('app/controller/auth'))

module.exports = (passport) => {
  passport.use('local-signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
  }, auth.localSignup ))

  passport.use('local-login', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
  }, auth.localLogin ))
}
