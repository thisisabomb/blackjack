let jwt = {
  type: 'bearer',
  expire_in: 15 * 60 * 1000,
  access_token_expire: '90 days',
  refresh_token_expire: '90 days',
  prefix_redis_key: 'jti-',
  jti_bytes_len: 4
}

module.exports = { jwt }
