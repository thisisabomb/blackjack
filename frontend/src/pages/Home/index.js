import React, { Component } from 'react';
import Button from '../../components/Button';
import '../../styles/homePage.css';

class HomePage extends Component {
    constructor(props){
        super(props)
        // console.log(props)
    }
    render() {
        const { classes } = this.props
        return (
            <div className="container">
                <div className="buttonContainer">
                    <div onClick={ () => this.props.onOpenModal()}>
                        <Button title="START GAME" />
                    </div>
                    <div onClick={ () => this.props.onOpenSignUpModal()}>
                        <Button title="SIGN UP" />
                    </div>
                </div>
            </div>    
        );
    }
}

export default HomePage
