import React, { Component } from 'react';
import ButtonCircle from '../../components/ButtonCircle';
// import Button from '../../components/Button';
import '../../styles/gamePage.css';
import ListCards from '../../components/Card/listCards'
import Modal from 'react-responsive-modal';
import ScoreBoard from '../../components/ScoreBoard'
import { API_SERVICE } from '../../constants'

class GamePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: props.openMessage,
            openModalScoreBoard: props.openModalScoreBoard,
            stat: []
        }
        // console.log(props)
    }

    componentWillUpdate = () => {
        console.log(this.props.openModalScoreBoard)
        console.log('inaa')
    }

    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    onCloseModalScoreBoard = () => {
        this.setState({ openModalScoreBoard: false });
    };

    openBoard = () => {
        console.log('board')
        this.setState({ isLoading: true })
        fetch(API_SERVICE + '/board', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer ' + localStorage.getItem('TOKEN_USER')
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data)
                this.setState({ openModalScoreBoard: true, stat: data })
            }).catch((error) => {
                console.error(error);
            });
    }

    render() {
        return (
            <div>
                <div className="gameContainer">
                    <div className="dealerContainer">
                        <ListCards dealer={true} hide={this.props.hide} cards={this.props.dealerCards} />
                        {!this.props.openMessage ? null :
                            <div className="score">
                                {this.props.dealerScore}
                            </div>
                        }
                    </div>
                    <div className="playerContainer">
                        <ListCards dealer={false} hide={this.props.hide} cards={this.props.playerCards} />
                        <div className="score">
                            {this.props.playerScore}
                        </div>
                    </div>
                    {this.props.restart ?
                        <div className="btn">
                            <div onClick={() => this.props.gameStart()}>
                                <ButtonCircle title="Restart" />
                            </div>
                            <div onClick={() => this.openBoard()}>
                                <ButtonCircle title="Score Board" />
                            </div>
                        </div> :
                        <div className="btn">
                            <div onClick={() => this.props.hitCards()}>
                                <ButtonCircle title="HIT" />
                            </div>
                            <div onClick={() => this.props.standCards()}>
                                <ButtonCircle title="STAND" />
                            </div>
                        </div>
                    }

                </div>
                <Modal open={this.state.open} onClose={this.onCloseModal} classNames={{ modal: 'modal' }}>
                    <span className="message">
                        {this.props.message}
                    </span>
                </Modal>

                <Modal open={this.state.openModalScoreBoard} onClose={this.onCloseModalScoreBoard} classNames={{ modal: 'scoreBoard' }}>
                    <ScoreBoard stat={this.state.stat} />
                </Modal>
            </div>
        );
    }
}

export default GamePage
