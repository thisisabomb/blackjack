import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import '../../styles/login.css'
import Button from '../Button'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    dense: {
        marginTop: 19,
    },
    input: {
        color: 'white'
    }
});

class ModalLogin extends Component {

    render() {
        const { classes } = this.props
        return (
            <div className="loginContainer">
                <div className="inputContainer">
                    <form className={classes.container} noValidate autoComplete="off">
                        <TextField
                            onChange={ (e) => this.props.handleUsername(e.target.value)}
                            id="standard-password-input"
                            label="Username"
                            className={classes.textField}
                            type="text"
                            margin="normal"
                            InputProps={{
                                style: {
                                    color: "white",
                                    // fontSize: 20
                                },
                            }}
                        />
                    </form>
                    <form className={classes.container} noValidate autoComplete="off">
                        <TextField
                            onChange={ (e) => this.props.handlePassword(e.target.value)}
                            id="standard-password-input"
                            label="Password"
                            className={classes.textField}
                            type="password"
                            autoComplete="current-password"
                            margin="normal"
                            InputProps={{
                                style: {
                                    color: "white",
                                    // fontSize: 20
                                },
                            }}
                        />
                    </form>
                    <div className="btn-login">
                        {/* <ListCards/> */}
                        <div onClick={() => this.props.fetchLogin()}>
                            <Button title="Login" />
                        </div>
                    </div>
                    {/* <Button/> */}
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(ModalLogin);
// export default ModalLogin;