import React, { Component } from 'react';
import Card from './card'
import '../../styles/listcards.css'

class ListCard extends Component {
    constructor(props){
        super()
        this.state = {
            // data: [{ type: "A", value: 11}, { type: "C", value: 10}],
            data: props.cards         
        }
        console.log('list', this.state.data)
    }

    componentWillUpdate = () => {
        // this.setState({ data: this.props.cards })
    }
    render() {
      return (
        <div className="Container">
            {this.state.data.map((data, index) => {
                const hide = (index === 0 && this.props.dealer && this.props.hide) ? true: false
                return (
                    <div className="CardContainer">
                        <Card type={data.type} value={data.value} hide={hide}/>
                    </div>
                )
            })}
        </div>
      );
    }
  }
  
  export default ListCard;