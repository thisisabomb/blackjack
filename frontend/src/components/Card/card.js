import React, { Component } from 'react';
import '../../styles/card.css'

class Card extends Component {
  constructor(props) {
    super()
    this.state = {
      type: props.type,
      value: props.value,
      symbol: '',
      text: '',
      symbolColor: '',
    }
  }

  componentWillMount() {
    this.selectType()
    this.selectText()
  }

  selectType() {
    const { type } = this.state
    if (type === 'A') {
      this.setState({ symbol: '♥', symbolColor: '#B22222' })
    } else if (type === 'B') {
      this.setState({ symbol: '♦', symbolColor: '#B22222' })
    } else if (type === 'C') {
      this.setState({ symbol: '♣', symbolColor: '#FAFAFA' })
    } else if (type === 'D') {
      this.setState({ symbol: '♠', symbolColor: '#FAFAFA' })
    }
  }

  selectText() {
    const { value } = this.state
    let text = ''
    if (value === 2) text = 'TWO'
    else if (value === 3) text = 'THREE'
    else if (value === 4) text = 'FOUR'
    else if (value === 5) text = 'FIVE'
    else if (value === 6) text = 'SIX'
    else if (value === 7) text = 'SEVEN'
    else if (value === 8) text = 'EIGHT'
    else if (value === 9) text = 'NINE'
    else if (value === 10) text = 'TEN'
    else if (value === 'J') text = 'JACK'
    else if (value === 'Q') text = 'QUEEN'
    else if (value === 'K') text = 'KING'
    else if (value === 11) {
      text = 'ACE'
      this.setState({ value: 'A' })
    }
    this.setState({ text })
  }


  render() {
    return (
      <div>
        {this.props.hide ? <div className="Card"/> :
          (
            <div className="Card">
              <div className="Top">
                <span style={{ color: this.state.symbolColor }}>{this.state.value}</span>
                <span className="Symbol" style={{ color: this.state.symbolColor }}>{this.state.symbol}</span>
              </div>
              <span className="TextCenter" style={{ color: this.state.symbolColor }}>{this.state.text}</span>
              <div className="Bottom">
                <span style={{ color: this.state.symbolColor }}>{this.state.value}</span>
                <span className="Symbol" style={{ color: this.state.symbolColor }}>{this.state.symbol}</span>
              </div>
            </div>
          )
        }
      </div>
    );
  }
}

export default Card;