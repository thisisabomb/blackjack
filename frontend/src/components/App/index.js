import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import '../../styles/main.css';
import style from '../../styles/login.css'
import ListCard from '../Card/listCards'
import HomePage from '../../pages/Home'
import GamePage from '../../pages/Game'
import ModalLogin from '../modalLogin'
import ModalSignUp from '../ModalSignUp'

import { API_SERVICE } from '../../constants'

class App extends Component {
  state = {
    playerCards: [],
    dealerCards: [],
    playerScore: '',
    dealerScore: '',
    isLoading: true,
    open: false,
    username: '',
    password: '',
    loginSuccess: false,
    hide: true,
    restart: false,
    openMessage: false,
    message: '',
    openModalScoreBoard: false,
    openSignup: false
  }

  componentDidMount = () => {
    // if (localStorage.getItem('TOKEN_USER')){
    //   this.setState({ loginSuccess: true })
    // }
    this.gameStart()
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onOpenSignUpModal = () => {
    this.setState({ openSignup: true });
  };

  onCloseSignUpModal = () => {
    this.setState({ openSignup: false });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  handleUsername = (text) => {
    console.log('username', text)
    this.setState({ username: text })
  }

  handlePassword = (text) => {
    console.log('password', text)
    this.setState({ password: text })
  }

  fetchLogin = () => {
    fetch(API_SERVICE + '/signin', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    })
      .then(res => {
        return res.json()
      })
      .then(data => {
        console.log(data)
        if (data.token.access) {
          localStorage.setItem('TOKEN_USER', data.token.access)
          this.setState({ loginSuccess: true, open: false })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  fetchSignUp = () => {
    fetch(API_SERVICE + '/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    })
      .then(res => {
        return res.json()
      })
      .then(data => {
        console.log(data)
        if (data.token.access) {
          localStorage.setItem('TOKEN_USER', data.token.access)
          this.setState({ loginSuccess: true, openSignup: false })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  gameStart = () => {
    this.setState({ isLoading: true, hide: true, restart: false, openMessage: false })
    fetch(API_SERVICE + '/cards/start', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + localStorage.getItem('TOKEN_USER')
      }
    }).then(res => res.json())
      .then(data => {
        // console.log(data.playerCard)
        this.setState({ playerCards: data.playerCard, dealerCards: data.dealerCard, isLoading: false, playerScore: data.playerScore, dealerScore: data.dealerScore })
        this.forceUpdate()
      }).catch((error) => {
        console.error(error);
      });
  }

  hitCards = () => {
    console.log('hit')
    this.setState({ isLoading: true })
    fetch(API_SERVICE + '/cards/hit', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + localStorage.getItem('TOKEN_USER')
      }
    }).then(res => res.json())
      .then(data => {
        console.log(data)
        if (!data.forceLoss) {
          this.setState({ playerCards: data.playerCard, isLoading: false, playerScore: data.playerScore })
          this.forceUpdate()
        }
        else {
          console.log(data)
          this.setState({ playerCards: data.playerCard, isLoading: false, playerScore: data.playerScore, message: data.message, openMessage: true, restart: true, hide: false })
          this.forceUpdate()
        }
      }).catch((error) => {
        console.error(error);
      });
  }

  standCards = () => {
    console.log('stand')
    this.setState({ isLoading: true })
    fetch(API_SERVICE + '/cards/stand', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + localStorage.getItem('TOKEN_USER')
      }
    }).then(res => res.json())
      .then(data => {
        console.log(data)
        this.setState({ hide: false, isLoading: false, restart: true, openMessage: true, message: data.message })
        this.forceUpdate()
      }).catch((error) => {
        console.error(error);
      });
  }

  openScoreBoard = () => {
    console.log('score')
    this.setState({ openModalScoreBoard: true })
    this.forceUpdate()
  }

  render() {
    console.log('Cards', this.state.playerCards)
    return (
      <div className="App">
        {/* <ListCard/> */}
        {!this.state.loginSuccess ? <HomePage onOpenModal={this.onOpenModal} onCloseModal={this.onCloseModal} onOpenSignUpModal={this.onOpenSignUpModal} onCloseSignUpModal={this.onCloseSignUpModal} /> :
            this.state.isLoading ? null :
              <GamePage
                hitCards={this.hitCards}
                standCards={this.standCards}
                gameStart={this.gameStart}
                playerCards={this.state.playerCards}
                dealerCards={this.state.dealerCards}
                playerScore={this.state.playerScore}
                dealerScore={this.state.dealerScore}
                hide={this.state.hide}
                restart={this.state.restart}
                openMessage={this.state.openMessage}
                message={this.state.message}
                openScoreBoard={this.openScoreBoard}
                openModalScoreBoard={this.state.openModalScoreBoard}
              />
        }
        <Modal open={this.state.open} onClose={this.onCloseModal} classNames={{
          modal: 'test',
        }}>
          <ModalLogin fetchLogin={this.fetchLogin} handleUsername={this.handleUsername} handlePassword={this.handlePassword} />
        </Modal>

        <Modal open={this.state.openSignup} onClose={this.onCloseSignUpModal} classNames={{
          modal: 'test',
        }}>
          <ModalSignUp fetchSignUp={this.fetchSignUp} handleUsername={this.handleUsername} handlePassword={this.handlePassword} />
        </Modal>
      </div>
    );
  }
}

export default App;
