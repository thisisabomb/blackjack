import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

let id = 0;
function createData(username, win, loss, draw) {
  id += 1;
  return { id, username, win, loss, draw };
}

let rows = []

class ScoreBoard extends Component {
    state = {
        data: this.props.stat,
        rows: []
    }
    componentDidMount = () => {
        rows = []
        this.createdRow()
    }

    createdRow = () => {
        this.state.data.map( data => {
            // console.log(data)
            // console.log('row',rows)
            rows.push(createData(data.profile.username, data.statistics.win, data.statistics.loss, data.statistics.draw))
            this.setState({ rows })
        })
    }

    render() {
        const { classes } = this.props
        return (
            <Paper className={classes.root}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Username</TableCell>
                    <TableCell align="right">Win</TableCell>
                    <TableCell align="right">Loss</TableCell>
                    <TableCell align="right">Draw</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.rows.map(row => (
                    <TableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.username}
                      </TableCell>
                      <TableCell align="right">{row.win}</TableCell>
                      <TableCell align="right">{row.loss}</TableCell>
                      <TableCell align="right">{row.draw}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
          );
    }
}


export default withStyles(styles)(ScoreBoard);