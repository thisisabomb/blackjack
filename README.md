# Blackjack

# Table of content
* [How to start Backend](#How to start Backend)
* [How to start Frontend](#How to start Frontend)
* [File structure](#File structure)


# How to start Backend
```
- Please open MongoDB on port 127.0.0.1:27017
cd backend/
npm install
npm start
```

# How to start Frontend
```
cd frontend/
npm install
npm start
```

# File structure
```
├── README.md
├── backend                     # core data service
├── frontend                    # front end 
```
